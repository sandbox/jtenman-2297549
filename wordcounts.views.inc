<?php

/**
 * @file
 * Views integration.
 */

/**
 * Implements hook_views_data().
 */
function wordcounts_views_data() {
  $data = array();
  $data['wordcounts']['table']['group'] = t('Content');
  $data['wordcounts']['table']['join'] = array(
    'node' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );
  $data['wordcounts']['wordcount'] = array(
    'title' => t('Word Count'),
    'help' => t('The number of words in this node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
